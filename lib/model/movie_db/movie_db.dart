import 'package:hive/hive.dart';

part 'movie_db.g.dart';

@HiveType(typeId: 0)
class MovieDB extends HiveObject{

  @HiveField(0)
  String title;

  @HiveField(1)
  String poster;

  @HiveField(2)
  String year;

  @HiveField(3)
  String type;

  MovieDB(
    this.title,
    this.poster,
    this.year,
    this.type
  );
}