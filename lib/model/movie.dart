class MovieModel{

  final String title; 

  final String poster;

  final String year;

  final String type;

  MovieModel({
    required this.title,
    required this.poster,
    required this.year,
    required this.type
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    return MovieModel(
      title: json["Title"], 
      poster: json["Poster"],
      year: json["Year"],
      type: json["Type"]
    );
  }
}
