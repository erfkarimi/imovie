import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movie/model/movie.dart';

class ApiService {

  Future<List<MovieModel>> fetchMovies(String keyword) async {

    final url = Uri.parse("http://www.omdbapi.com/?s=$keyword&apikey=API_KEY");
    final response = await http.get(url);
    if(response.statusCode == 200) {

       final body = jsonDecode(response.body); 
       final Iterable json = body["Search"];
       return json.map((movie) => MovieModel.fromJson(movie)).toList();

    } else {
      throw Exception("Unable to perform request!");
    }
  }
}