import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetTheme with ChangeNotifier{
  String theme = "light";

  Color appBarBackgroundTheme(){
    switch(theme){
      case "dark": {
        return Colors.grey.shade900;
      }
      case "dim": {
        return const Color.fromRGBO(20, 29, 38, 1.0);
      }   
      default: {
        return const Color.fromRGBO(0, 106, 106, 1.0);
      }
    }
  }

  Color scaffoldBackgroundTheme(){
    switch(theme){
      case "dark": {
        return const Color(0xFF32373b);
      }
      case "dim": {
        return const Color.fromRGBO(36, 52, 71, 1.0);
      }   
      default: {
        return Colors.white;
      }
    }
  }

  Color searchBarBackgroundTheme(){
    switch(theme){
      case "dark": {
        return const Color(0xFF32373b);
      }
      case "dim": {
        return const Color.fromRGBO(36, 52, 71, 1.0);
      }   
      default: {
        return Colors.white;
      }
    }
  }

  Color buttonTheme(){
    switch(theme){
      case "dark": {
        return Colors.white;
      }
      case "dim": {
        return Colors.white;
      }   
      default: {
        return Colors.black;
      }
    }
  }

  Color itemBackgroundTheme(){
    switch(theme){
      case "dark": {
        return Colors.grey.shade700;
      }
      case "dim": {
        return Colors.blueGrey.shade700;
      }   
      default: {
        return Colors.grey.shade300;
      }
    }
  }

  Color themeBottomSheetTheme(){
    switch(theme){
      case "dark": {
        return Colors.grey.shade900;
      }
      case "dim": {
        return const Color.fromRGBO(36, 52, 71, 1.0);
      }   
      default: {
        return Colors.white;
      }
    }
  }


  
  Future<void> changeTheme() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("theme", theme);
    notifyListeners();
  }
  
    
}