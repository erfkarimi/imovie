import 'package:flutter/foundation.dart';
import 'package:movie/service/api_service.dart';

import 'movie_model_view.dart';

class MovieListViewModel extends ChangeNotifier {

  List<MovieViewModel> movies = <MovieViewModel>[]; 

  Future<void> fetchMovies(String keyword) async {
    try{
    final results =  await ApiService().fetchMovies(keyword);
    movies = results.map((item) => MovieViewModel(movie: item)).toList();
    } catch (_){}
    notifyListeners(); 
  }

}