import 'package:movie/model/movie.dart';

class MovieViewModel {

  final MovieModel movie; 

  MovieViewModel({required this.movie});

  String get title => movie.title;

  String get poster => movie.poster;

  String get year => movie.year;

  String get type => movie.type;
}