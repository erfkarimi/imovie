import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:movie/l10n/l10n.dart';
import '../view/splash_screen/splash_screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
class App extends StatelessWidget{
  const App({super.key});

  @override 
  Widget build(context){
    return MaterialApp(
      theme: ThemeData(
        fontFamily: "Lato",
      ),
      debugShowCheckedModeBanner: false,
      supportedLocales: L10n.all,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
      home: const SplashScreen(),
    );
  }
}