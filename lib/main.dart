import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:movie/model/movie_db/movie_db.dart';
import 'package:movie/model_view/movie_list_view.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:provider/provider.dart';
import 'app/app.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  var appDocumentDirectory = 
      await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(MovieDBAdapter());
  await Hive.openBox<MovieDB>("game");
  await Hive.openBox<MovieDB>("movie");
  await Hive.openBox<MovieDB>("series");

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_)=> MovieListViewModel()),
          ChangeNotifierProvider(
          create: (context)=> SetTheme()),
      ],
      child: const App()
    ));
}