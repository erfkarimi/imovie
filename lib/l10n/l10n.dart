import 'package:flutter/material.dart';

class L10n{
  static final all = [
    const Locale("de", "DE"),
    const Locale("en", "US"),
    const Locale("es", "ES"),
    const Locale("fa", "IR"),
  ];

  static String getFlag(String code){
    switch(code){
      case "de": {
        return "🇩🇪";
      }
      case "es": {
        return "🇪🇸";
      }
      case "fa": {
        return "🇮🇷";
      }
      default: 
       return "🇺🇸";
    }
  }
}