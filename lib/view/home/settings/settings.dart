import 'package:flutter/material.dart';
import 'package:movie/l10n/l10n.dart';
import 'package:provider/provider.dart';
import '../../../model_view/theme/theme.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class Settings extends StatefulWidget{
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  
  /// -----------------------------------------------
  /// :                                             :
  /// :             Widgets & Functions             :
  /// :                                             :
  /// -----------------------------------------------

  @override 
  Widget build(context){
    return buildScaffold();
  }

  /* Scaffold */
  Widget buildScaffold(){
    final SetTheme setTheme = Provider.of<SetTheme>(context);
    final locale = Localizations.localeOf(context);
    final flag = L10n.getFlag(locale.languageCode);
    return Scaffold(
      backgroundColor: setTheme.scaffoldBackgroundTheme(),
      appBar: buildAppBar(setTheme, flag),
      body: buildBody(setTheme)
    );
  }

  /* AppBar */
  AppBar buildAppBar(SetTheme setTheme, String flag){
    return AppBar(
      backgroundColor: setTheme.appBarBackgroundTheme(),
      elevation: 0.0,
      title: const Text(
        "Settings",
        style: TextStyle(
          color: Colors.white
        )
      ),
      actions: [
        Center(
          child: Text(
            flag,
            style: const TextStyle(
              fontSize: 20
            )
          ),
        ),
        const SizedBox(
          width: 20,
        ),
      ]
    ); 
  }

  /* Body */
  Widget buildBody(SetTheme setTheme){
    
    
    return Column(
      children: [
        themeButton(setTheme, context),
        feedbackButton(context, setTheme)
        
      ]
    );
  }

  Widget themeButton(SetTheme setTheme, BuildContext context){
    return MaterialButton(
      onPressed: (){
        showThemeBottomSheet(context, setTheme);
      },
      child: ListTile(
        leading: Icon(
          Icons.brightness_2_outlined,
          color: setTheme.buttonTheme()),
        title: Text(
          AppLocalizations.of(context)!.themeButtonTitle,
          style: TextStyle(
            color: setTheme.buttonTheme()
          ),
          ),
      )
    );
  }

  Widget lightThemeButton(SetTheme setTheme, BuildContext context){
    return MaterialButton(
      minWidth: 200,
      onPressed: (){
        setTheme.theme = "light";
        setTheme.changeTheme();
        Navigator.of(context).pop();
      },
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0.0,
      child: const Text(
        "Light",
        style: TextStyle(
          color: Colors.black
        )
        )
    );
  }

  Widget darkThemeButton(SetTheme setTheme, BuildContext context){
    return MaterialButton(
      minWidth: 200,
      onPressed: (){
        setTheme.theme = "dark";
        setTheme.changeTheme();
        Navigator.of(context).pop();
      },
      color: Colors.grey.shade900,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0.0,
      child: const Text(
        "Dark",
        style: TextStyle(
          color: Colors.white
        )
        )
    );
  }

  Widget dimThemeButton(SetTheme setTheme, BuildContext context){
    return MaterialButton(
      minWidth: 200,
      onPressed: (){
        setTheme.theme = "dim";
        setTheme.changeTheme();
        Navigator.of(context).pop();
      },
      color: const Color.fromRGBO(36, 52, 71, 1.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0.0,
      child: const Text(
        "Dim",
        style: TextStyle(
          color: Colors.white
        )
        )
    );
  }


  void showThemeBottomSheet(BuildContext context, SetTheme setTheme){
    showModalBottomSheet(
      context: context,
      builder: (context){
        return BottomSheet(
          onClosing: (){},
          backgroundColor: setTheme.themeBottomSheetTheme(),
          enableDrag: false,
          builder: (context){
            return SizedBox(
              height: 227,
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    Container(
                      height: 4,
                      width: 100,
                      decoration: BoxDecoration(
                        color: setTheme.buttonTheme(),
                        borderRadius: BorderRadius.circular(20)
                      ),
                    ),
                    const SizedBox(height: 10),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        AppLocalizations.of(context)!.themeButtonTitle,
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: setTheme.buttonTheme()
                        )
                        ),
                    ),
                    const SizedBox(height: 10),
                    lightThemeButton(setTheme, context),
                    darkThemeButton(setTheme, context),
                    dimThemeButton(setTheme, context)
                  ],
                ),
              )
            );
          }
          );
      }
    );
  }

  Widget feedbackButton(BuildContext context, SetTheme setTheme){
    return MaterialButton(
      onPressed: (){
        _sendMail();
      },
      child: ListTile(
        leading: Icon(
          Icons.question_mark,
          color: setTheme.buttonTheme()
          ),
        title: Text(
          AppLocalizations.of(context)!.feedbackButtonTitle,
          style: TextStyle(
            color: setTheme.buttonTheme()
          ),
          ),
      )
    );
  }

  Future<void> _sendMail() async{
  final uri = Uri.parse('mailto:kberfan99@gmail.com?subject=Need help');
    await launchUrl(uri);
  
  }
}


