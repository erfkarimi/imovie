import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:movie/view/home/favorite/game/game.dart';
import 'package:movie/view/home/favorite/movie/movie.dart';
import 'package:movie/view/home/favorite/series/series.dart';
import 'package:provider/provider.dart';
import '../../../model/movie_db/movie_db.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Favorite extends StatefulWidget{
  const Favorite({super.key});

  @override 
  LikedState createState() => LikedState();
}

class LikedState extends State<Favorite>{

  /// -----------------------------------------------
  /// :                                             :
  /// :             Widgets & Functions             :
  /// :                                             :
  /// -----------------------------------------------

  @override 
  Widget build(context){
    return buildScaffold();
  }

  /* Scaffold */
  Widget buildScaffold(){
    final SetTheme setTheme = Provider.of<SetTheme>(context);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: setTheme.scaffoldBackgroundTheme(),
        appBar: buildAppBar(setTheme),
        body: buildBody()
      ),
    );
  }


  /* App bar */
  AppBar buildAppBar(SetTheme setTheme){
    return AppBar(
      backgroundColor: setTheme.appBarBackgroundTheme(),
      elevation: 0.0,
      title: Text(
        AppLocalizations.of(context)!.favoriteButton,
        style: const TextStyle(
          color: Colors.white
        )
      ),
      bottom: const TabBar(
        indicatorColor: Colors.white,
          indicatorWeight: 3.0,
          labelPadding: EdgeInsets.only(bottom : 10),
        tabs: [
          Text(
            "Game",
            style: TextStyle(
          color: Colors.white
         )
          ),
          Text(
            "Movie",
            style: TextStyle(
          color: Colors.white
         )
          ),
          Text(
            "Series",
            style: TextStyle(
          color: Colors.white
         )
          ),
          
        ],
      )
    );
  }

  /* Body */
  Widget buildBody(){
    return const TabBarView(
      children: [
        Game(),
        Movie(),
        Series()
        ]
      );
  }
}