import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:provider/provider.dart';
import '../../../../model/movie_db/movie_db.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Game extends StatefulWidget{
  const Game({super.key});

  @override 
  GameState createState()=> GameState();
}
class GameState extends State<Game>{
  final Box<MovieDB> gameBox = Hive.box<MovieDB>("game");
  @override 
  Widget build(context){
    final SetTheme setTheme = Provider.of<SetTheme>(context);
    
    return showSavedItem(setTheme);
  }

  /* Show saved item */
    Widget showSavedItem(SetTheme setTheme){
    return ValueListenableBuilder<Box<MovieDB>>(
     valueListenable: gameBox.listenable(),
      builder: (context, movieBox, _){
        if(movieBox.isEmpty){
          return showNoItem(setTheme);
        } else {
          return ListView.builder(
            itemCount: movieBox.length,
            itemBuilder: (context, int index){
            return Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                padding: const EdgeInsets.only(
                  bottom: 15, top: 5
                  ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: setTheme.itemBackgroundTheme(),
                ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                       Flexible(
                        child: ListTile(
                          title: Text(
                            movieBox.getAt(movieBox.length - 1 - index)!.title,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                              color: setTheme.buttonTheme()
                            ),
                            ),
                            subtitle: Row(
                              children: [
                                Text(
                                movieBox.getAt(movieBox.length - 1 - index)!.year,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Text(
                                  movieBox.getAt(movieBox.length - 1 - index)!.type,
                                  style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                  )
                                  )
                              ],
                            ),
                            trailing: IconButton(
                              icon: const Icon(
                                Icons.delete_outline,
                                size: 27,
                                color: Colors.red,
                                ),
                              onPressed: (){
                                movieBox.deleteAt(movieBox.length - 1 - index);
                              },
                            ),
                        ),
                      ),
                      Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)
                        ),
                        child: Image.network(
                          movieBox.getAt(movieBox.length - 1 - index)!.poster,
                          fit: BoxFit.cover,
                          errorBuilder: ((context, error, stackTrace){
                            return Image.asset("asset/image/monster-404-error.png");
                          }),
                          ),
                      ),
                    ],
                  ),
              ),
            );
            }
        );
      }
    }
    );
  }


  Widget showNoItem(SetTheme setTheme){
    return Center(
      child: 
            Text(
              AppLocalizations.of(context)!.noItemMessage,
              style: TextStyle(
                color: setTheme.buttonTheme(),
                fontSize: 17,
                letterSpacing: 2.0,
                wordSpacing: 5.0,
                height: 2,
                inherit: false
              )
            )
    );
  }
}