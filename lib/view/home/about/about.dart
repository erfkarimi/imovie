import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../model_view/theme/theme.dart';

class About extends StatefulWidget{
  const About({super.key});

  @override 
  AboutState createState()=> AboutState();
}

class AboutState extends State<About>{
  @override 
  Widget build(context){
    return buildScaffold();
  }

  /* Scaffold */
  Widget buildScaffold(){
    final SetTheme setTheme = Provider.of<SetTheme>(context);
    return Scaffold(
      backgroundColor: setTheme.scaffoldBackgroundTheme(),
      appBar: buildAppBar(setTheme),
      body: buildBody(setTheme),
    );
  }

  /* AppBar */
  AppBar buildAppBar(SetTheme setTheme){
    return AppBar(
      backgroundColor: setTheme.appBarBackgroundTheme(),
      elevation: 0.0,
      title: const Text(
        "About",
        style: TextStyle(
          color: Colors.white
        )
      )
    );
  }

  /* Body */
  Widget buildBody(SetTheme setTheme){
    return Center(
      child: Text(
        """
Thanks for choosing iMovie
  this app helps you to 
  find your favorite game,
  movie and series.
        """,
        style: TextStyle(
          color: setTheme.buttonTheme(),
          letterSpacing: 2.0,
          wordSpacing: 5.0,
          height: 2,
          inherit: false
        ),
      )
    );
  }
}
