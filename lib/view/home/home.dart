import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:movie/view/home/favorite/favorite.dart';
import 'package:movie/view/home/settings/settings.dart';
import 'package:provider/provider.dart';
import 'about/about.dart';
import 'search/search.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class Home extends StatefulWidget{
  const Home({super.key});

  @override
  HomeState createState()=> HomeState();
}

class HomeState extends State<Home>{

  /* Variables & Instances */
  int _selectedIndex = 0;

  List<Widget> items = [
    const Favorite(),
    const Search(),
    const About(),
    const Settings()
    
  ];
  
  /// -----------------------------------------------
  /// :                                             :
  /// :             Widgets & Functions             :
  /// :                                             :
  /// -----------------------------------------------

  @override 
  void initState() {
    super.initState();
    setStatusTheme();
  }

  void onItemTap(int index){
    setState(() {
      _selectedIndex = index;
    });
  }
  
  
  @override
  Widget build(context){
    return DefaultTabController(
      length: 3,
      child: buildScaffold()
    );
  }

  /* Scaffold */
  Widget buildScaffold(){
    final setTheme = Provider.of<SetTheme>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
        bottomNavigationBar: bottomNavigationBar(setTheme),
        body: buildBody()
      );
  } 


  /* Body */
  Widget buildBody(){
    return Center(
      child: items[_selectedIndex]
    );
  }

  /* Navigation Bar */
  Widget bottomNavigationBar(SetTheme setTheme){
    return BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: onItemTap,
      elevation: 0.0,
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white,
      items: [
        BottomNavigationBarItem(
          backgroundColor: setTheme.appBarBackgroundTheme(),
          icon: const Icon(Icons.favorite_outline),
          activeIcon: const Icon(Icons.favorite),
          label: AppLocalizations.of(context)!.favoriteButton
        ),
        BottomNavigationBarItem(
          backgroundColor: setTheme.appBarBackgroundTheme(),
          icon: const Icon(Icons.search_outlined),
          activeIcon: const Icon(Icons.search_outlined),
          label: AppLocalizations.of(context)!.searchButtonTitle
        ),
        BottomNavigationBarItem(
          backgroundColor: setTheme.appBarBackgroundTheme(),
          icon: const Icon(Icons.info_outline),
          activeIcon: const Icon(Icons.info),
          label: AppLocalizations.of(context)!.aboutButtonTitle
        ),
        BottomNavigationBarItem(
          backgroundColor: setTheme.appBarBackgroundTheme(),
          icon: const Icon(Icons.settings_outlined),
          activeIcon: const Icon(Icons.settings),
          label: AppLocalizations.of(context)!.settingsButton
        )
      ]
      
      );
  }

  void setStatusTheme(){
    final SetTheme setTheme = Provider.of<SetTheme>(context, listen: false);
    setState(() {
      SystemChrome.setSystemUIOverlayStyle(
       SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: setTheme.appBarBackgroundTheme(),
        statusBarColor: Colors.transparent,
      ));     
    });
  }
  
}