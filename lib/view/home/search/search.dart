import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:movie/model/movie_db/movie_db.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:movie/view/loading/loading.dart';
import 'package:provider/provider.dart';
import '../../../model_view/movie_list_view.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
class Search extends StatefulWidget{
  const Search({super.key});

  @override 
  SearchState createState() => SearchState(); 
}

class SearchState extends State<Search>{
  final TextEditingController _textController = TextEditingController();
  bool saved = false;
  bool loading = false;

  /// -----------------------------------------------
  /// :                                             :
  /// :             Widgets & Functions             :
  /// :                                             :
  /// -----------------------------------------------

  @override
  Widget build(BuildContext context) {
    return  buildScaffold();
  }

  Widget buildScaffold(){
    final SetTheme setTheme = Provider.of<SetTheme>(context);
    return Scaffold(
      backgroundColor: setTheme.scaffoldBackgroundTheme(),
      resizeToAvoidBottomInset: false,
      appBar: buildAppBar(setTheme),
      body: loading ? const Loading() : showItem(setTheme),
    );
  }

  AppBar buildAppBar(SetTheme setTheme){
    return AppBar(
     backgroundColor: setTheme.appBarBackgroundTheme(),
      elevation: 0.0,
      title: searchTextField(setTheme, context),
    );
  }

  
  Widget showItem(SetTheme setTheme){
    final gameBox = Hive.box<MovieDB>("game");
    final movieBox = Hive.box<MovieDB>("movie");
    final seriesBox = Hive.box<MovieDB>("series");
    return Consumer<MovieListViewModel>(
      builder: (context, movieListViewModel, _){
        List movies = movieListViewModel.movies;
        if(movies.isEmpty){                  
          return noItemWidget();
        }
        return ListView.builder(
          itemCount: movies.length,
          itemBuilder: (context, int index){
            String poster = movies[index].poster;
            return Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                padding: const EdgeInsets.only(
                  bottom: 15, top: 5
                  ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: setTheme.itemBackgroundTheme(),
                ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                       Flexible(
                        child: ListTile(
                          title: Text(
                            movies[index].title,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                              color: setTheme.buttonTheme()
                            ),
                            ),
                            subtitle: Row(
                              children: [
                                Text(
                                movies[index].year,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Text(movies[index].type,
                                  style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                  )
                                  )
                              ],
                            ),
                            trailing: IconButton(
                              splashColor: Colors.red,
                              icon: const Icon(
                                Icons.favorite_outline,
                                size: 27,
                                color: Colors.red,
                              ),
                              onPressed: (){
                                if(movies[index].type == "game"){
                                  gameBox.add(
                                    MovieDB(
                                      movies[index].title,
                                      movies[index].poster,
                                      movies[index].year,
                                      movies[index].type
                                    ));
                                  setState(() {
                                    movies.removeAt(index);
                                  });
                                } else if(movies[index].type == "movie"){
                                    movieBox.add(
                                      MovieDB(
                                        movies[index].title,
                                        movies[index].poster,
                                        movies[index].year,
                                        movies[index].type
                                  ));
                                  setState(() {
                                    movies.removeAt(index);
                                  });
                                } else {
                                  seriesBox.add(
                                    MovieDB(
                                        movies[index].title,
                                        movies[index].poster,
                                        movies[index].year,
                                        movies[index].type
                                    )
                                  );
                                  setState(() {
                                    movies.removeAt(index);
                                  });
                                }
                              },
                            ),
                        ),
                      ),
                      Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)
                        ),
                        child: Image.network(
                          poster,
                          fit: BoxFit.cover,
                          errorBuilder: ((context, error, stackTrace){
                            return Image.asset("asset/image/monster-404-error.png");
                          }),
                          ),
                      ),
                     
                    ],
                  ),
              ),
            );
            }
        );
      } 
    );
  }

    Widget searchTextField(SetTheme setTheme, BuildContext context){
    return Container(
      height: 40,
      padding: const EdgeInsets.only(left: 20),
      decoration: BoxDecoration(
        color: setTheme.searchBarBackgroundTheme(),
        borderRadius: BorderRadius.circular(20)
      ),
      child: Consumer<MovieListViewModel>(
        builder: (context, movieListViewModel, _) {
          return TextField(
            controller: _textController,
            cursorColor: setTheme.buttonTheme(),
            style: TextStyle(color: setTheme.buttonTheme()),
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context)!.textFieldHint,
              hintStyle: TextStyle(color: setTheme.buttonTheme()),
              border: InputBorder.none,
              suffixIcon: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    icon: Icon(
                     Icons.close,
                     size: 20, color: setTheme.buttonTheme()),
                    onPressed: (){
                      setState(() {
                        _textController.clear();
                        movieListViewModel.movies.clear();
                      });
                    },
                  ),
                   Container(
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Colors.blue,
                          Colors.red
                        ]
                      ),
                      borderRadius: BorderRadius.circular(20)
                    ),
                     child: MaterialButton(
                      height: double.infinity,
                      minWidth: 70,
                      elevation: 0.0,
                      onPressed: (){
                        if(_textController.text.isNotEmpty){
                          FocusScope.of(context).unfocus();
                          showLoading();
                          movieListViewModel.fetchMovies(_textController.text);
                        }
                      },
                      color: Colors.transparent,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20)
                        )
                      ),
                      child: const Icon(
                        Icons.arrow_forward,
                        color: Colors.white,),
                                     ),
                   ),
                ],
              )
            ),
          );
        }
      ),
    );
  }


  Future<void> showLoading() async {
    bool connection = await checkInternetConnection();
    if (connection == true) {
      setState(() {
        loading = true;
      });
    }
    await Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        loading = false;
      });
    });
  }


  Future<bool> checkInternetConnection() async{
    try{
      await InternetAddress.lookup('google.com');
      return true;
    } on SocketException catch(_){
        showConnectionSnackBar();
        return false;
    }
  }


  void showConnectionSnackBar(){
    SnackBar snackBar = SnackBar(
      content: Row(
        children: const [
          Icon(
            Icons.cloud_off_outlined,
            color: Colors.white,),
            SizedBox(width: 10),
          Text("No connection"),
        ],
      ),
      duration: const Duration(seconds: 2),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }


  Widget noItemWidget(){
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            bottom: 100
          ),
          child: Center(
            child: Image.asset(
              "asset/image/file-searching.png",
              width: 280
              ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            right: 20, bottom: 30),
          child: Center(
            child: Text(
              AppLocalizations.of(context)!.searchMessage,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 15
              )
              ),
          ),
        )
      ],
    );
  }
}