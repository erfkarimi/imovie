import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import '../home/home.dart';

class SplashScreen extends StatefulWidget{
  const SplashScreen({super.key});

  @override 
  SplashScreenState createState()=> SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>{
  
  @override
  void initState() {
    super.initState();
    setTheme();
    init(context);
    navigation();
  }

  /// -----------------------------------------------
  /// :                                             :
  /// :             Widgets & Functions             :
  /// :                                             :
  /// -----------------------------------------------  

  @override 
  Widget build(context){
    return buildBody();
  }


  Widget buildBody(){
    return const Material(
      color: Color.fromRGBO(0, 106, 106, 1.0),
      child: Center(
        child: Text(
          "iMovie",
          style: TextStyle(
            fontSize: 40,
            color: Colors.white,
            fontWeight: FontWeight.bold
          )
          )),
    ); 
  }


  void setTheme(){
    setState(() {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Color.fromRGBO(0, 106, 106, 1.0),
        statusBarColor: Colors.transparent,
      ));     
    });
  }


  Future<void> init(context) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final setTheme = Provider.of<SetTheme>(context, listen: false);
    setState(() {
      setTheme.theme = preferences.getString("theme") ?? "light";
    });
  }


  Timer navigation(){
    return Timer(const Duration(seconds: 2),
      (){
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context)=> const Home())
        );
      }
    );
  }
}