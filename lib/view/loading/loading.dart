import 'package:flutter/material.dart';
import 'package:movie/model_view/theme/theme.dart';
import 'package:provider/provider.dart';

class Loading extends StatelessWidget{
  const Loading({super.key});

  @override 
  Widget build(context){
    final SetTheme setTheme = Provider.of<SetTheme>(context);
    return Material(
      color: setTheme.scaffoldBackgroundTheme(),
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(setTheme.buttonTheme()),
        ),
      ),
    );
  }
}